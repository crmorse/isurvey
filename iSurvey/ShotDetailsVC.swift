
import UIKit
import CoreData
import MapKit

class ShotDetailsVC: UIViewController, UITextFieldDelegate, UITextViewDelegate {
    
    var shot: Shot?
    
    @IBOutlet weak var station: UITextField!
    @IBOutlet weak var created: UITextField!
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var value: UITextField!
    
    @IBOutlet weak var notes: UITextView!
    
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        station.text = shot?.fromStation?.displayName
        created.text = (shot?.created != nil) ? CDM.dateFormatter?.string(from: shot!.created!) : ""
        name.text = shot?.displayName
        value.text = shot?.debugDescription
        
        notes.text = shot?.memo
        
        if let coord = shot?.location?.coordinate {
            mapView.centerCoordinate = coord
        } else {
            mapView.isHidden = true
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //        if let vc = segue.destinationViewController as? StationDetailsVC {
        //            stationVC.station = sender as? Station
        //        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        
        return true
    }
}
