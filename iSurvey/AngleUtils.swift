//
//  AngleUtils.swift
//
//  Created by Chris Morse on 2/22/15.
//
//  BSD License

import Foundation

enum AngleUnits: String, CustomStringConvertible {
    case Degrees = "d"
    case Gons = "g"
    case Mils = "m"

    var description: String {
        switch self {
        case .Degrees:  return "Degrees"
        case .Gons:     return "Gons"
        case .Mils:     return "Mils"
        }
    }
}

enum DegreesUnits: CustomStringConvertible {
    case dms
    case decimal
    
    var description: String {
        switch self {
        case .dms:      return "DMS"
        case .decimal:  return "Decimal"
        }
    }
}

enum AngleType: String, CustomStringConvertible {
    case Horizontal = "h"
    case Vertical = "v"
    case Slope = "s"
    
    var description: String {
        return self.rawValue
    }
}

enum AngleValue {
    case decimal(Double)
    case dms(Int, Int, Double)
    
    var decimalValue: Double {
        switch self {
        case .decimal(let decimal):
            return decimal
        case .dms(let d, let m, let s):
            let decimal: Double = Double(d) + (Double(m)/60.0) + (s/3600.0)
            return decimal
        }
    }
    
    var dmsValue: (d: Int, m: Int, s: Double) {
        switch self {
        case .decimal(let decimal):
            var angle = decimal
            
            //Separate degrees and subtract from total
            let d: Int = Int(floor(angle))
            angle -= Double(d)
            
            //Separate minutes and subtract from total
            let m: Int = Int(floor(angle * 60.0))
            angle -= (Double(m) / 60.0)
            
            //Separate seconds and keep remainder
            let s: Double = angle * 3600.0;
            
            return (d: d, m: m, s: s)
        case .dms(let d, let m, let s):
            let dms: (d: Int, m: Int, s: Double) = (d: d, m: m, s: s)
            return dms
        }
    }
}

struct Angle: CustomStringConvertible {
    var units: AngleUnits
    var value: AngleValue
    
    //RegEx Patterns in angle tokens
    static var Degrees      = "([+-]?\\d{3})([ ]?[Ddº]?[ ]?)"
    static var Minutes      = "(\\d{2})([ ]?[Mm']?[ ]?)"
    static var Seconds      = "(\\d{2})([\\.]?\\d?)([ ]?[Ss\"]?[ ]?)([dmg]?)"
    
    //Create from decimal format
    init(decimal: Double, units: AngleUnits) {
        self.units = units
        self.value = .decimal(decimal)
    }
    
    /// Create from DMS format
    init(degrees: Int, minutes: Int, seconds: Double) {
        units = .Degrees
        value = .dms(degrees, minutes, seconds)
    }
    
    /// Create from string with implied decimal units for Degrees format only
    init?(string: String) {
        //This could get hairy:
        //  It might or might not have a sign (S: +/-)
        //  It might or might not have formatting chars (D:[Ddº], M:[Mm'] S:[Ss"])
        //  It might or might not have tenths of seconds (s: [0-9])
        //
        //Topcon format is ±DDDMMSSsd (no decimal character)
        //
        let pattern = Angle.Degrees     // 0: Degrees
            //                          // 1: degree formatting symbol
            + Angle.Minutes             // 2: Minutes
            //                          // 3: minutes formatting symbol
            + Angle.Seconds             // 4: Seconds
        //                          // 5: Tenths
        //                          // 6: seconds formatting symbol
        //                          // 7: Angle Units symbol
        
        if let tokens = tokenizeResponse(string, pattern: pattern) {
            let D = Int(tokens[0])
            let M = Int(tokens[2])
            let S = tokens[4].toDouble()
            let t = tokens[5].toDouble()
            
            //Optional unwrapping is such a pain
            switch (D, M, S, t) {
            case (.some, .some, .some, .some):
                let tenths = t! / 10.0
                self = Angle(degrees: D!, minutes: M!, seconds: S!+tenths)
                return
            case (.some, .some, .some, .none):
                self = Angle(degrees: D!, minutes: M!, seconds: S!)
                return
            default:
                return nil
            }
        }
        return nil
    }
    
    
    func toString(_ degreesUnits: DegreesUnits = .dms) -> String {
        switch degreesUnits {
        case .dms:
            switch value {
            case .dms(let d, let m, let s):
                return "\(d)º\(m)'\(s)\""
            case .decimal(let decimal):
                return "\(decimal)"
            }
        case .decimal:
            return "" //\(angle)"
        }
    }
    
    var description: String {
        return toString()
    }
}

