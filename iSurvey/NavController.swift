import Foundation
import MessageUI

var NC: NavController?

class NavController: UINavigationController, UIAlertViewDelegate, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate {
    
    var alertVC: UIAlertController?

    override func viewDidLoad() {
        NC = self
        super.viewDidLoad()
        self.navigationBar.isTranslucent = false
        
    }

    func wantsNavigationBarVisible(_ visible: Bool) {
        self.setNavigationBarHidden(!visible, animated: true)
    }
    
    //MARK: - Workflow helpers
    func dismissModalAlert() {
        if let _ = alertVC {
            self.dismiss(animated: true, completion: { () -> Void in
                self.alertVC = nil
            })
        }
    }
    
    //MARK: - Universal Alert handler
    func showAlert(_ title: String? = nil, message: String? = nil, cancelButtonTitle: String? = "Dismiss") {
        DispatchQueue.main.async(execute: { () -> Void in
            let alert = UIAlertView(title: title, message: message, delegate: nil, cancelButtonTitle: cancelButtonTitle)
            alert.show()
        })
    }

    func showFatalAlert(_ title: String? = nil, message: String? = nil, cancelButtonTitle: String? = "CLOSE APP") {
        DispatchQueue.main.async(execute: { () -> Void in
            let alert = UIAlertView(title: title, message: message, delegate: self, cancelButtonTitle: cancelButtonTitle)
            alert.show()
        })
    }
    
    //MARK: - UIAlertViewDelegate
    func alertView(_ alertView: UIAlertView, didDismissWithButtonIndex buttonIndex: Int) {
        //All alerts with delegate set are fatal.
        abort()
    }

    //MARK: - MFMailComposeViewControllerDelegate
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - MFMessageComposeViewControllerDelegate
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
