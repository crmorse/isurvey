
import Foundation

let EM = ErrorManager.sharedInstance

open class ErrorManager: NSObject {
    static let sharedInstance = ErrorManager()
    private override init() {}

    private func formatMessage(_ description: String? = nil, error: NSError? = nil) -> String {
        var message = ""
        switch (description, error) {
        case (.some, .some):    message = "\(description!). Cause: \(error!.localizedDescription)"
        case (.some, .none):    message = description!
        case (.none, .some):    message = error!.localizedDescription
        default:                message = "Unknown cause"
        }
        return message
    }
    
    func reportError(_ source: String, description: String? = nil, error: NSError? = nil) {
//        Analytics.tagEvent("Error", attributes: ["source": source, "description": description ?? "", "error": error?.localizedDescription ?? ""])
        
        let message = formatMessage(description, error: error)
        print("ERROR: \(message)")
        
        NC?.showAlert("Error from \(source)", message: message)
    }

    func fatalError(_ source: String, description: String? = nil, error: NSError? = nil) {
//        Analytics.tagEvent("Error", attributes: ["source": source, "description": description ?? "", "error": error?.localizedDescription ?? "", "fatal": true])
        
        let message = formatMessage(description, error: error)
        print("ERROR: \(message)")
        
        NC?.showFatalAlert("Fatal Error from \(source)", message: message)
    }
}
