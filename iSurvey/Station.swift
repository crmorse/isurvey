//
//  Station.swift
//  iSurvey
//
//  Created by Chris Morse on 2/22/15.
//  Copyright (c) 2015 Morse Technologies, Inc. All rights reserved.
//

import Foundation
import CoreData
import CoreLocation

class Station: NSManagedObject {

    //Logistics
    @NSManaged var location: CLLocation?
    @NSManaged var created: Date?
    
    //Station description
    @NSManaged var name: String?
    @NSManaged var memo: String?
    
    //Relationships
    @NSManaged var measurements: NSSet
    @NSManaged var backsight: Shot?
    @NSManaged var foresight: Shot?
    
    override func awakeFromInsert() {
        super.awakeFromInsert()
        
        location = LocationManager.sharedInstance.currentLocation
        created = Date()
    }
    
    var displayName: String {
        var displayName = (name != nil) ? name : memo

        if (displayName == nil) {
            let num = objectID.uriRepresentation().lastPathComponent
            displayName = "Station #\(num)"
        }
        
        return displayName!
    }
    
    var displayDetails: String {
        let date = (created != nil) ? CDM.dateFormatter?.string(from: created!) : ""
        let display = "\(date!): \(measurements.count) shots"
        return display
    }
}
