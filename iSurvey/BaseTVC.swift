import UIKit
import CoreData

let BaseTableViewCell: String = "BaseTableViewCell"

class BaseTVC: UITableViewController, NSFetchedResultsControllerDelegate {

    var fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult>!
    var hasChanges: Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: BaseTableViewCell)
    }
    
    // This function will be called when the Dynamic Type user setting changes (from the system Settings app)
    func contentSizeCategoryChanged(_ notification: Notification) {
        //TODO: AddObserver for this Notification
        self.reloadTableData()
    }

    func configureFetchedResultsController(_ fetchRequest: NSFetchRequest<NSFetchRequestResult>, sectionNameKeyPath: String? = nil) {
        self.fetchedResultsController = NSFetchedResultsController(
            fetchRequest: fetchRequest,
            managedObjectContext: CDM.mainThreadContext,
            sectionNameKeyPath: sectionNameKeyPath,
            cacheName: nil)

        self.fetchedResultsController.delegate = self
    }
    
    func reloadTableData() {
        let selectedPaths = tableView.indexPathsForSelectedRows
        tableView.reloadData()
        
        if let selectedPaths = selectedPaths {
            for indexPath in selectedPaths {
                tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
            }
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        if let sectionInfo = fetchedResultsController?.sections {
            return sectionInfo.count
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let sectionInfo = fetchedResultsController?.sections {
            if sectionInfo.count > section {
                return sectionInfo[section].numberOfObjects
            }
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: BaseTableViewCell, for: indexPath)
        let apiObject = fetchedResultsController?.object(at: indexPath)
//        cell.textLabel?.text = apiObject?.name
        return cell
    }
    
    @IBAction func refreshRequested(_ sender: UIRefreshControl) {
//        dataSignal.deliverOnMainThread().subscribeError({ (error) -> Void in
            sender.endRefreshing()
//            EM.reportError("Refreshing", error: error)
//        }, completed: { () -> Void in
//            sender.endRefreshing()
//            do {
//                if self.fetchedResultsController != nil {
//                    try self.fetchedResultsController.performFetch()
//                }
//            } catch {
//                nop()
//            }
            self.reloadTableData()
//        })
    }

    // MARK: - NSFetchedResultsControllerDelegate
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        print("PGGBaseTVC:controllerDidChangeContent()")
        self.reloadTableData()
    }
}


