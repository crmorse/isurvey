//
//  RedparkDriver.swift
//  iSurvey
//
//  Created by Chris Morse on 2/14/15.
//  Copyright (c) 2015 Morse Technologies, Inc. All rights reserved.
//

import Foundation

typealias ResponseBlock = (ResponsePacket) -> Void
typealias LoggerBlock = (String) -> Void
typealias ConnectionObserverBlock = (Bool) -> Void

class RedparkDriver: NSObject, RscMgrDelegate {
 
    var rscMgr:RscMgr!
    
    var portConfig: serialPortConfig!
    var isConnected: Bool = false {
        didSet {
            connectionObserver(isConnected)
        }
    }
    
    var rxBuffer: NSString = NSString()
    
    var readerBlock: ResponseBlock = { (_) in }
    var loggerBlock: LoggerBlock = { (log) in debugPrint(log) }
    var connectionObserver: ConnectionObserverBlock = { (_) in }
    
    override init() {
        rscMgr = RscMgr()
//        portConfig = serialPortConfig(baudLo: 0, baudHi: 0, dataLen: SERIAL_DATABITS_8, parity: SERIAL_PARITY_NONE, stopBits: STOPBITS_1, txFlowControl: TXFLOW_NONE, rxFlowControl: RXFLOW_NONE, xonChar: 0, xofChar: 0, rxForwardingTimeout: 500, rxForwardCount: 2048, txActSetting: 0, returnStatus: 0)
        
        super.init()
        rscMgr.setDelegate(self)
    }
    
    func setupRedparkSerial() {
        rscMgr.setBaud(9600)
        rscMgr.setDataSize(kDataSize8)
        rscMgr.setParity(kParityNone)
        rscMgr.setStopBits(kStopBits1)
        rscMgr.setRts(true)
        rscMgr.setDtr(true)
    }
    
    func writePacket(_ request: RequestPacket) {
        loggerBlock("# write: \(request.requestString)")
        
//        let bytes = [UInt8](request.requestString.utf8)
//        let bytePtr: UnsafeMutablePointer = UnsafeMutablePointer(bytes)
//        rscMgr.write("C067\r\n", length: 7)

//        if let data = request.requestString.dataUsingEncoding(NSUTF8StringEncoding) {
//            rscMgr.writeData(data)
//        }

        rscMgr.write(request.requestString)
        loggerBlock("> \(request.requestString)")
    }
    
//    func readPacket(block: ResponseBlock) {
//        readerBlock = block
//    }
    
    // MARK: - RscMgrDelegate methods
    
    // Redpark Serial Cable has been connected and/or application moved to foreground.
    // protocol is the string which matched from the protocol list passed to initWithProtocol:
    func cableConnected(_ proto: String) {
        setupRedparkSerial()
        rscMgr.open()
        isConnected = true
    }
    
    // Redpark Serial Cable was disconnected and/or application moved to background
    func cableDisconnected() {
        isConnected = false
    }
    
    // serial port status has changed
    // user can call getModemStatus or getPortStatus to get current state
    func portStatusChanged() {
        if var config = self.portConfig {
            rscMgr.getPortConfig(&config)
            loggerBlock("# Config: \(config.baudHi)\(config.baudLo)\(config.dataLen)\(config.parity)\(config.stopBits)>>\(config.txFlowControl)/\(config.rxFlowControl)")
        }
    }
    
    // bytes are available to be read (user should call read:, getDataFromBytesAvailable, or getStringFromBytesAvailable)
    func readBytesAvailable(_ length: UInt32) {
        // Accumulate the received data
        let temp = rscMgr.getStringFromBytesAvailable()
        accumulateString(temp!)
    }

    func accumulateString(_ inString: String) {
        rxBuffer = rxBuffer.appending(inString) as NSString
//        loggerBlock("# readBytes: \(inString)\n")
        
        //Find the end of the first packet
        let terminatorRange = (rxBuffer as NSString).range(of: "\n")
        if terminatorRange.location != NSIntegerMax {
            //Grab it out and leave remainder for next loop
            let rawPacket = rxBuffer.substring(with: NSMakeRange(0, terminatorRange.location+1))

            let remainder = NSMakeRange(terminatorRange.location+1, rxBuffer.length-terminatorRange.location-1)
            rxBuffer = rxBuffer.substring(with: remainder) as NSString
//            loggerBlock("# parsed rawPacket: \(rawPacket)")
            
            //Parse the response
            let response = ResponsePacket(rawResponse: rawPacket)
            loggerBlock("< \(response.rawString)\n")
            
            //Send back reply
            let reply = response.isValid ? RequestPacket.ACK() : RequestPacket.NAK()
            self.writePacket(reply)
            
            //Write out the results
            readerBlock(response)
        }
    }
    
    // called when a response is received to a getPortConfig call
//    func didReceivePortConfig () {
//        
//    }
}
