//
//  Shot.swift
//  iSurvey
//
//  Created by Chris Morse on 2/22/15.
//  Copyright (c) 2015 Morse Technologies, Inc. All rights reserved.
//

import Foundation
import CoreData
import CoreLocation

class Shot: NSManagedObject {

    @NSManaged var rawString: String?
    
    //Logistics
    @NSManaged var created: Date?
    @NSManaged var name: String?
    @NSManaged var location: CLLocation?
    @NSManaged var fromStation: Station?
    @NSManaged var backsight: Station?
    

    //Angles
    @NSManaged var angleUnits: String?
    @NSManaged var vAngle: NSNumber?
    @NSManaged var hAngle: NSNumber?
    @NSManaged var sAngle: NSNumber?
    
    //Distances
    @NSManaged var distanceUnits: String?
    @NSManaged var hDistance: NSNumber?
    @NSManaged var vDistance: NSNumber?
    @NSManaged var sDistance: NSNumber?
    
    //Coordinates
    @NSManaged var northing: NSNumber?
    @NSManaged var easting: NSNumber?
    @NSManaged var elevation: NSNumber?
    
    //Instrument setttings
    @NSManaged var tiltCorrection: NSNumber?
    @NSManaged var edmLevel: NSNumber?
    @NSManaged var edmPPM: NSNumber?
    @NSManaged var prismConstant: NSNumber?
    @NSManaged var instrumentHeight: NSNumber?
    @NSManaged var prismHeight: NSNumber?
    
    //Supplemental fields
    @NSManaged var memo: String?
    
    override func awakeFromInsert() {
        super.awakeFromInsert()
        
        location = LocationManager.sharedInstance.currentLocation
        created = Date()
    }
    
    func apply(_ response: ResponsePacket) {
        rawString       = response.rawString as String
        
        angleUnits      = response.angleUnits?.rawValue
        vAngle          = response.verticalAngle?.value.decimalValue as NSNumber?? ?? NSNumber()
        hAngle          = response.horizontalAngle?.value.decimalValue as NSNumber?? ?? NSNumber()
//        sAngle          = response.slopeAngle?.value.decimalValue //Not implemented
        
        distanceUnits   = response.distanceUnits?.rawValue
        hDistance       = response.horizontalDistance as NSNumber?? ?? NSNumber()
        vDistance       = response.verticalDistance as NSNumber?? ?? NSNumber()
        sDistance       = response.slopeDistance as NSNumber?? ?? NSNumber()
        northing        = response.northing as NSNumber?? ?? NSNumber()
        easting         = response.easting as NSNumber?? ?? NSNumber()
        elevation       = response.elevation as NSNumber?? ?? NSNumber()

        tiltCorrection  = response.tiltCorrection?.boolValue as NSNumber?? ?? NSNumber()
        edmLevel        = response.edmSignalLevel as NSNumber?? ?? NSNumber()
        edmPPM          = response.edmPPMCorrection as NSNumber?? ?? NSNumber()
        prismConstant    = response.prismConstant as NSNumber?? ?? NSNumber()
        instrumentHeight = response.instrumentHeight as NSNumber?? ?? NSNumber()
        prismHeight     = response.prismHeight as NSNumber?? ?? NSNumber()
    }
    
    var displayName: String {
        var displayName = (name != nil) ? name : memo
        
        if (displayName == nil) {
            let num = objectID.uriRepresentation().lastPathComponent
            displayName = "Shot #\(num)"
        }
        
        return displayName!
    }

    override var debugDescription: String {
        return displayDetails
    }
    
    var displayDetails: String {
        let formatter = CDM.numberFormatter!
        var desc = ""
        
        //Angles
        desc += (hAngle != nil)             ? " Hº:\(formatter.string(from: hAngle!)!)" : ""
        desc += (vAngle != nil)             ? " Vº:\(formatter.string(from: vAngle!)!)" : ""
        //Distances
        desc += (hDistance != nil)          ? " HD:\(formatter.string(from: hDistance!)!)" : ""
        desc += (vDistance != nil)          ? " VD:\(formatter.string(from: vDistance!)!)" : ""
        //Coordinates
        desc += (northing != nil)           ? "  N:\(formatter.string(from: northing!)!)" : ""
        desc += (easting != nil)            ? "  E:\(formatter.string(from: easting!)!)" : ""
        desc += (elevation != nil)          ? "  Z:\(formatter.string(from: elevation!)!)" : ""
        //Instrumentation
        desc += (tiltCorrection != nil)     ? "  t:\(formatter.string(from: tiltCorrection!)!)" : ""
        desc += (edmLevel != nil)           ? "  L:\(formatter.string(from: edmLevel!)!)" : ""
        desc += (edmPPM != nil)             ? "  P:\(formatter.string(from: edmPPM!)!)" : ""
        desc += (prismConstant != nil)      ? "  O:\(formatter.string(from: prismConstant!)!)" : ""
        desc += (instrumentHeight != nil)   ? " IH:\(formatter.string(from: instrumentHeight!)!)" : ""
        desc += (prismHeight != nil)        ? " PH:\(formatter.string(from: prismHeight!)!)" : ""

        return desc
    }
    
}
