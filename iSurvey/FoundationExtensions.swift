//
//  FoundationExtensions.swift
//
//  Created by Chris Morse on 2/22/15.
//
//  BSD License

import Foundation

infix operator ** { associativity left precedence 170 }
func ** (num: Double, power: Double) -> Double{
    return pow(num, power)
}

extension String {
    
    /// Parses a double precision number out of a String with an optional number of implied decimal places.  (This is common in older fixed-width formats)
    func toDouble(_ places: Int = 0) -> Double? {
        let formatter = NumberFormatter()
        formatter.positiveFormat = "+"
        formatter.negativeFormat = "-"
        formatter.isLenient = true
        
        let number = formatter.number(from: self)

        if var value = number?.doubleValue {
            //Incoming number had implied decimal places (like a fixed width format)
            value = value / (10 ** Double(places))
            return value
        }
        return nil
    }
}
