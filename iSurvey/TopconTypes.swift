//
//  TopconTypes.swift
//  iSurvey
//
//  Created by Chris Morse on 2/5/15.
//  Copyright (c) 2015 Morse Technologies, Inc. All rights reserved.
//

import Foundation

enum DistanceUnits: String, CustomStringConvertible {
    case Feet = "f"
    case Meters = "m"
    
    var description: String {
        switch self {
        case .Feet:     return "Feet"
        case .Meters:   return "Meters"
        }
    }
}

enum Measurement: String, CustomStringConvertible {
    case HorizontalDistance = "h"
    case VerticalDistance = "v"
    case SlopeDistance = "s"
    
    var description: String {
        switch self {
        case .HorizontalDistance:   return "HD"
        case .VerticalDistance:     return "VD"
        case .SlopeDistance:        return "SD"
        }
    }
}

enum DistanceMeasureMode: String, CustomStringConvertible {
    case Tracking = "1"
    case CourseSingle = "2"
    case CourseRepeat = "3"
    case FineSingle = "4"
    case FineRepeat = "5"
    
    func defaultResolution(_ command: RequestType) -> DistanceMeasureMode {
        switch command {
        case .modeSD, .modeHD, .modeVD, .modeNE, .modeZ:
            return .CourseSingle
        default:
            return .Tracking
        }
    }
    
    var description: String {
        switch self {
        case .Tracking:     return "Tracking"
        case .CourseSingle: return "Course Single"
        case .CourseRepeat: return "Course Repeating"
        case .FineRepeat:   return "Fine Repeating"
        case .FineSingle:   return "Fine Single"
        }
    }
}

enum TiltCorrection: String, CustomStringConvertible {
    case Yes = "t"
    case No = "*"
    
    var boolValue: Bool {
        return (self == .Yes)
    }
    
    var description: String {
        switch self {
        case .Yes:  return "Yes"
        case .No:   return "No"
        }
    }
}

///RegEx patterns that match tokens in the Response Sentences
struct Patterns {
    //Patterns in response sentences
    static let ID           = "^(.)"
    static let Angle        = "([+-]?\\d*)([dms]?)"
    static let Distance     = "([+-]?\\d*)([mf]?)"
    static let DistanceUnit = "([mf]{1})"
    static let AngleUnit    = "([dms]?)"
    static let AngleType    = "([shv]?)"
    static let Tilt         = "([t\\*]{1})"
    static var XTilt        = "([+-]?\\d{4})"
    static var EDMSignal    = "([\\*\\d*]{2})"
    static var EDMppm       = "([+-]?\\d{2,4})"
    static let Prism        = "([+-]?\\d{2,4})"
    static var Unused       = "(\\*{2})"
    static var Unknown      = "(.*)"
    static let BCC          = "(\\d{3})"
    static let Terminator   = "(.){0,3}$"                       // Swift 3 hack
    //    static let Terminator   = "(␃)"  //"(.\\r\\n)$"      // <---This doesn't work either; not sure why not
    //    static let Terminator   = "(.\\r\\n)?$"               // <--- broke in Swift 3
}

func calculateBCC(_ data: String) -> String {
    //5-2-3. How to make a BCC. (from the Topcon GTS-750 Interface Manual)
    // Step 1: Treat each character as a 7/8-bit ASCII code.
    // Step 2: Make bitwise EXCLUSIVE-OR of the Null code (initially which is a NULL code of the ASCII 00H) and the first character.
    // Step 3: Regard the result as the next step used.
    // Step 4: Make bitwise EXCLUSIVE-OR of the result at step 3 and the next character.
    // Step 5: Repeat the step 3 to the step 4 until the final character is operated.
    // Step 6: Convert the result of the 7/8-bit code to a 3-digit decimal number.
    
    var rawBcc: UInt8 = ASCIIControlCharacter.nul.rawValue
    for byte in data.unicodeScalars {
        let next = UInt8(byte.value)
        rawBcc = rawBcc ^ next
    }
    
    //TODO: Need number formatter
    let string = String(format: "%03d", rawBcc)
    return string as String
}

