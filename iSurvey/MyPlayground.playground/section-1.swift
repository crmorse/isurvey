// Playground - noun: a place where people can play

import Foundation

// MARK: - Swift bugs
/*
let haystack: NSString = "006newline\r\n"
let idx = haystack.rangeOfString("\n")
if idx.location != NSIntegerMax {
    println(idx)
} else {
    println("Oh swift, I hate thee")
}


var foo = true
foo != true

let x = "s"
let bytes = [UInt8](x.utf8)[0]

//let y: UInt8 =
//println("y=\(y); y+y=\(y+y)")
*/

//MARK: - NSNumberFormatter
/*
infix operator ** { associativity left precedence 170 }
func ** (num: Double, power: Double) -> Double{
return pow(num, power)
}
extension String {
func toDouble(places: Int = 0) -> Double? {
let formatter = NSNumberFormatter()
formatter.format = "+;0;-"
formatter.lenient = true
let number = formatter.numberFromString(self)

if var value = number?.doubleValue {
//Incoming number had implied decimal places (like a fixed width format)
value = value / (10 ** Double(places))
return value
}
return nil
}
}

"00006145".toDouble(places: 4)
"+00006145".toDouble(places: 4)
"-00006145".toDouble(places: 4)
let double: Double = 6145 / (10 ** Double(4))
*/

// MARK: - String splitting
/*
extension NSString {
    public func splitOn(separator: NSCharacterSet) -> [NSString] {
        var components = [String]()
        var searchRange: NSRange = NSMakeRange(0, self.length)
        
        while (searchRange.location != NSNotFound) {
            var foundRange = self.rangeOfCharacterFromSet(separator, options: .LiteralSearch, range: searchRange)
            
            if (foundRange.location != NSNotFound) {
                var tokenRange = NSMakeRange(max(0,searchRange.location-1),
                    foundRange.location-searchRange.location+1)
                
                components.append(self.substringWithRange(tokenRange))
                searchRange.location = foundRange.location+1
                searchRange.length = self.length - searchRange.location
            } else {
                //take whatever is left of the string and quit
                var tokenRange = NSMakeRange(searchRange.location, self.length-searchRange.location)
                components.append(self.substringWithRange(tokenRange))
                break
            }
        }
        
        return components
    }
}
*/

//var response: NSString = "?+00006145f0060612+1701650d+00006110t60+06+00040rt087\u{3}\r\n"
//var response: NSString = "(-000061440f00605500-170164601+000061090t**+0060+000**030rt041"
//var pattern: NSString = "^(.)([+-]?\\d*)([fm])([+-]?\\d*)([+-]?\\d*)([+-]?\\d*)(t)(..)([+-]?\\d*)([+-]?\\d*)(\\*.*t)(\\d{3})"
//let pattern = Patterns.ID    0: ResponseType
//    + Patterns.Distance      1: SD
//                             2: distance units
//    + Patterns.Angle         3: VAngle
//                             4: (missing units token)
//    + Patterns.Angle         5: HAngle
//                             6: angle units
//    + Patterns.Distance      7: HD
//                             8: (missing units token)
//    + Patterns.Tilt          9: Tilt
//    + Patterns.EDMSignal    10: EDM signal level
//    + Patterns.EDMppm       11: EDM PPM
//    + Patterns.Prism        12: Prism constant
//    + Patterns.Unknown      13: Unknown data
//    + Patterns.BCC          14: BCC
//    + Patterns.Terminator   15: ␃␍␊


///Takes the response sentence and parses it using the given regex `pattern`.
///
/// Returns: array of string tokens
func tokenizeResponse(response: String, pattern: String) -> [String]? {
    do {
        var tokens = [String]()
        let range = NSMakeRange(0, response.characters.count)
        let regex = try NSRegularExpression(pattern: pattern)
        debugPrint("regex: \(regex)")

        regex.enumerateMatches(in: response, range: range, using: { (result, flags, stop) -> Void in
            if let tokenCount = result?.numberOfRanges {
                for i in 1 ..< tokenCount {
                    let token = (response as NSString).substring(with: result!.rangeAt(i))
                    tokens.append(token)
                }
            }
        })

        return tokens
    } catch let error {
        print("Exception creating regular expression with pattern: \(pattern) \n Exception: \(error)")
        return nil
    }
}

struct Patterns {
    static let ID           = "^(.)"
    static let Angle        = "([+-]?\\d*)([dms]?)"
    static let Distance     = "([+-]?\\d*)([mf]?)"
    static let DistanceUnit = "([mf]{1})"
    static let AngleUnit    = "([dms]?)"
    static let AngleType    = "([shv]?)"
    static let Tilt         = "([t\\*]{1})"
    static let XTilt        = "([+-]?\\d{4})"
    static let EDMSignal    = "([\\*\\d*]{2})"
    static let EDMppm       = "([+-]?\\d{4})"
    static let Prism        = "([+-]?\\d{4})"
    static let Unused       = "(\\*{2})"
    static let Unknown      = "(.*)"
    static let BCC          = "(\\d{3})"
    static let Terminator   = "(.){0,3}$"  //"(.\\r\\n)$"
    
    //Patterns in angle tokens
    static let Degrees      = "([+-]?\\d{3})([ ]?[Ddº]?[ ]?)"
    static let Minutes      = "(\\d{2})([ ]?[Mm']?[ ]?)"
    static let Seconds      = "(\\d{2})([\\.]?\\d?)([ ]?[Ss\"]?[ ]?)([dmg]?)"
}

//MARK: - DMS Parsing
let pattern = Patterns.ID   // 0: ResponseType
    + Patterns.Angle        // 1: HAngle
    //                      // 2: (missing units token)
    + Patterns.Distance     // 3: N
    //                      // 4: (missing units token)
    + Patterns.Distance     // 5: E
    //                      // 6: (missing units token)
    + Patterns.Distance     // 7: Z
    //                      // 8: distance units
    + Patterns.Distance     // 9: SO - Stake Out Distance
    //                      //10: distance units
    + Patterns.AngleType    //11: SO distance type (slope/horizontal/vertical)
    + Patterns.Distance     //12: I - Instrument Height
    //                      //13: (missing units token)
    + Patterns.Distance     //14: P - Prism Height
    //                      //15: distance units
    + Patterns.BCC          //18: BCC
    + Patterns.Terminator   //19: ␃␍␊

//tokenizeResponse("W+000061400f00606060+17016500d-0000106300+0000092650+0000006300t60+0060+000040rt054\u{3}\r\n", pattern)
tokenizeResponse(response: "L+00000000-0000046100+0000082350-0000000250f+002220000fv+0010110+0020220f103\u{3}\r\n", pattern: pattern)


// MARK: - Angles
/*
enum DegreesUnits: Printable {
    case DMS
    case Decimal
    
    var description: String {
        switch self {
        case .DMS:      return "DMS"
        case .Decimal:  return "Decimal"
        }
    }
}

enum AngleValue {
    case decimal(Double)
    case dms(Int, Int, Double)
    
    var decimalValue: Double {
        switch self {
        case .decimal(let decimal):
            return decimal
        case .dms(let d, let m, let s):
            var decimal: Double = Double(d) + (Double(m)/60.0) + (s/3600.0)
            return decimal
        }
    }
    
    var dmsValue: (d: Int, m: Int, s: Double) {
        switch self {
        case .decimal(let decimal):
            var angle = decimal
            
            //Separate degrees and subtract from total
            let d: Int = Int(floor(angle))
            angle -= Double(d)
            
            //Separate minutes and subtract from total
            let m: Int = Int(floor(angle * 60.0))
            angle -= (Double(m) / 60.0)
            
            //Separate seconds and keep remainder
            let s: Double = angle * 3600.0;
            
            return (d: d, m: m, s: s)
        case .dms(let d, let m, let s):
            var dms: (d: Int, m: Int, s: Double) = (d: d, m: m, s: s)
            return dms
        }
    }
}

struct Angle: Printable {
    var value: AngleValue
    
    //Create from decimal format
    init(decimal: Double) {
        value = .decimal(decimal)
    }
    
    /// Create from DMS format
    init(degrees: Int, minutes: Int, seconds: Double) {
        value = .dms(degrees, minutes, seconds)
    }
    
    func toString(degreesUnits: DegreesUnits = .DMS) -> String {
        switch degreesUnits {
        case .DMS:
            switch value {
            case .dms(let d, let m, let s):
                return "\(d)º\(m)'\(s)\""
            case .decimal(let decimal):
                return "\(decimal)"
            }
        case .Decimal:
            return "" //\(angle)"
        }
    }
    
    var description: String {
        return toString()
    }
}

let a = Angle(degrees: 170, minutes: 15, seconds: 28)
a.description
a.value.decimalValue
a.value.dmsValue

let b = Angle(decimal: a.value.decimalValue)
b.description
b.value.decimalValue
b.value.dmsValue
*/

