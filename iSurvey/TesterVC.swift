//
//  TesterVC.swift
//  iSurvey
//
//  Created by Chris Morse on 2/4/15.
//  Copyright (c) 2015 Morse Technologies, Inc. All rights reserved.
//

import UIKit
import CoreData


class TesterVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var connectSegmented: UISegmentedControl!
    @IBOutlet weak var connectModeSegmented: UISegmentedControl!
    
    @IBOutlet weak var hModeSegmented: UISegmentedControl!
    @IBOutlet weak var measureModeSegmented: UISegmentedControl!
    @IBOutlet weak var resolutionSegmented: UISegmentedControl!

    @IBOutlet weak var commandSegmented: UISegmentedControl!
    
    @IBOutlet weak var locationTextField: UITextField!
    @IBOutlet weak var sentTextField: UITextField!
    @IBOutlet weak var receivedTextField: UITextField!
    @IBOutlet weak var loggerTextView: UITextView!
    @IBOutlet weak var responseTextView: UITextView!
    
    var configChanged = true
    var tsConfigRequest = RequestPacket(type: .recallData)
    
    var tsDistanceMode:DistanceMeasureMode!
    
    let driver = RedparkDriver()
    var station: Station?
    
    func getStation() -> Station {
        if (station == nil) {
            station = NSEntityDescription.insertNewObject(forEntityName: "Station", into: CDM.mainThreadContext) as? Station
            CDM.saveMainThreadContext()
        }
        return station!
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loggerTextView.text = nil
        responseTextView.text = nil
        
        hModeChanged(hModeSegmented)
        resolutionChanged(resolutionSegmented)
        measureModeChanged(measureModeSegmented)
        
        driver.readerBlock = { (response) in
            self.receivedTextField.text = response.rawString as String
            
            if (response.responseType != ResponseType.ack) {
                self.responseTextView.text = response.prettyPrintedDescription
                
                if (response.isValid) {
                    let shot = NSEntityDescription.insertNewObject(forEntityName: "Shot", into: CDM.mainThreadContext) as! Shot
                    shot.apply(response)
                    shot.fromStation = self.getStation()
                    CDM.saveMainThreadContext()
                }
            }
        }
        
        driver.loggerBlock = { (log) in
            var text: String = self.loggerTextView.text
            text += log
            self.loggerTextView.text = text
            
            let range = NSMakeRange(text.characters.count-1, 1)
            self.loggerTextView.scrollRangeToVisible(range)
            print(log, terminator: "")
        }
        
        driver.connectionObserver = { (connected) in
            self.connectSegmented.selectedSegmentIndex = (connected) ? 0 : 1
        }
    }
    
    func sendCommand(_ request: RequestPacket) {
        //send config request
        sentTextField.text = request.debugDescription
        if (connectModeSegmented.selectedSegmentIndex == 0) {
            driver.writePacket(request)
        } else {
            var x: String!
            switch (tsConfigRequest.type) {
            case .modeVH:                       x = ResponseType.vhAngleMode.sampleResponse
            case .modeSD(mode: tsDistanceMode): x = ResponseType.sdMode.sampleResponse
            case .modeHD(mode: tsDistanceMode): x = ResponseType.hDorVDMode.sampleResponse
            case .modeVD(mode: tsDistanceMode): x = ResponseType.hDorVDMode.sampleResponse
            case .modeNE(mode: tsDistanceMode): x = ResponseType.nezCoordinatesExpandedMode.sampleResponse
            case .modeZ (mode: tsDistanceMode): x = ResponseType.nezCoordinatesExpandedMode.sampleResponse
            default:            return //do nothing
            }
            driver.accumulateString(x)
        }
    }

    @IBAction func hModeChanged(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0: tsConfigRequest.type = .modeHL
        case 1: tsConfigRequest.type = .modeHR
        default: assertionFailure("Unexpected hMode segment")
        }
        configChanged = true
    }
    
    @IBAction func measureModeChanged(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0: tsConfigRequest.type = .modeVH
        case 1: tsConfigRequest.type = .modeSD(mode: tsDistanceMode)
        case 2: tsConfigRequest.type = .modeHD(mode: tsDistanceMode)
        case 3: tsConfigRequest.type = .modeVD(mode: tsDistanceMode)
        case 4: tsConfigRequest.type = .modeNE(mode: tsDistanceMode)
        case 5: tsConfigRequest.type = .modeZ (mode: tsDistanceMode)
        default: assertionFailure("Unexpected resolution segment")
        }
        configChanged = true
    }
    
    @IBAction func resolutionChanged(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0: tsDistanceMode = .Tracking
        case 1: tsDistanceMode = .CourseSingle
        case 2: tsDistanceMode = .CourseRepeat
        case 3: tsDistanceMode = .FineSingle
        case 4: tsDistanceMode = .FineRepeat
        default: assertionFailure("Unexpected measureMode segment")
        }
        //Have to update measureMode too
        measureModeChanged(measureModeSegmented)
        configChanged = true
    }
    
    @IBAction func startAction(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            sendCommand(RequestPacket(type: .recallData))
            
        case 1:
            if (configChanged) {
                sendCommand(tsConfigRequest)
                configChanged = false
            }
            sendCommand(RequestPacket(type: .startMeasuring))

        case 2:
            sendCommand(RequestPacket(type: .stopTracking))

        default: assertionFailure("Unexpected sendCommand segment")
        }
    }

    // MARK: - UITextFieldDelegate Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}

