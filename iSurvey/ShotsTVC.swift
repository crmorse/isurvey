import UIKit
import CoreData


class ShotsTVC: BaseTVC {
    
    var station: Station!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Shot")
        fetchRequest.predicate = NSPredicate(format: "fromStation = %@", self.station)
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "created", ascending: false)]
        
        self.configureFetchedResultsController(fetchRequest)
        
        do {
            try self.fetchedResultsController.performFetch()
        } catch let error as NSError {
            EM.reportError("Shot List", error: error)
        }
    }
    
    @IBAction func newShot(_ sender: UIBarButtonItem) {
        self.performSegue(withIdentifier: "newShot", sender: station)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? ShotDetailsVC {
            vc.shot = sender as? Shot
        } else if let vc = segue.destination as? TesterVC {
            vc.station = sender as? Station
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let shot = self.fetchedResultsController.object(at: indexPath)
        self.performSegue(withIdentifier: "showShot", sender: shot)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShotCell", for: indexPath)
        let shot = fetchedResultsController?.object(at: indexPath) as! Shot
        
        cell.textLabel?.text = shot.displayName
        cell.detailTextLabel?.text = shot.displayDetails
        
        return cell
    }
}

