//
//  CDManager.swift
//  PatriotGroundGame
//
//  Created by Chris Morse on 12/11/14.
//  Copyright (c) 2014 Tea Party Patriots. All rights reserved.
//

import Foundation
import CoreData

let CDM = CDManager.sharedInstance

open class CDManager: NSObject {

    static let sharedInstance = CDManager()
    private override init() {}

    var reuseableContext: NSManagedObjectContext?

    lazy var dateFormatter: DateFormatter? = {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "US_en")
        formatter.dateStyle = .short
        formatter.timeStyle = .short
        
        return formatter
    }()
    
    lazy var numberFormatter: NumberFormatter? = {
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "US_en")
        formatter.maximumFractionDigits = 2
        
        return formatter
    }()
    
    // MARK: - Core Data stack
    lazy var applicationDocumentsDirectory: URL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.razeware.HitList" in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1] as URL
        }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: "iSurvey", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
        }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator? = {
        // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        var error: NSError? = nil
        
        //Check for launch argument to clean the DB
//        if (UIApplication.launchArgument("FORCE_CLEAN_START")) { CDManager.deleteDB() }

        //Create coordinator
        var coordinator: NSPersistentStoreCoordinator? = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("iSurvey.sqlite")
        let options = [
            NSMigratePersistentStoresAutomaticallyOption: true,
            NSInferMappingModelAutomaticallyOption: true]
        
        do {
            try coordinator!.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: options)
        } catch var error1 as NSError {
            error = error1
            coordinator = nil
            // Report any error we got.
            let dict:[String:AnyObject] = [
                NSLocalizedDescriptionKey: "Failed to initialize the application's saved data" as AnyObject,
                NSLocalizedFailureReasonErrorKey: "There was an error creating or loading the application's saved data." as AnyObject,
                NSUnderlyingErrorKey: error!]
            error = NSError(domain: "PGG", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(error), \(error!.userInfo)")
            abort()
        } catch {
            fatalError()
        }
        
        return coordinator
        }()
    
    lazy var mainThreadContext: NSManagedObjectContext! = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        if let coordinator = self.persistentStoreCoordinator {
            var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
            managedObjectContext.persistentStoreCoordinator = coordinator
            return managedObjectContext
        } else {
            return nil
        }
    }()
    
    func backgroundContext() -> NSManagedObjectContext {
        if (reuseableContext == nil) {
            let managedObjectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
            managedObjectContext.persistentStoreCoordinator = self.persistentStoreCoordinator!
            managedObjectContext.mergePolicy = NSMergePolicy(merge: .mergeByPropertyObjectTrumpMergePolicyType)
            
//            let mergeSignal = NSNotificationCenter.defaultCenter()
//                .rac_addObserverForName(NSManagedObjectContextDidSaveNotification, object: managedObjectContext)
//            
//    //                .deliverOn(RACScheduler.mainThreadScheduler())
//            mergeSignal.subscribeNext({ (notification) -> Void in
//                if let notification = notification as? NSNotification {
//                    self.mainThreadContext?.mergeChangesFromContextDidSaveNotification(notification)
//                    managedObjectContext.mergeChangesFromContextDidSaveNotification(notification)
//                }
//            })
            
            print("WARNING: Creating a new background context: \(managedObjectContext)")
            return managedObjectContext
//            reuseableContext = managedObjectContext
        }
        return reuseableContext!
    }
    
    class func deleteDB() {
        /** Delete the sqlite database upon startup - for testing **/
        let fileManager = FileManager.default
        let urls = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
        let url = (urls[urls.count-1] as URL).appendingPathComponent("iSurvey.sqlite")
        let filePath = url.relativePath
        
        if (fileManager.fileExists(atPath: filePath)) {
            print("Deleting existing sqlite database")
            let _ = try? fileManager.removeItem(atPath: filePath)
        }	
    }

    
    // MARK: - Core Data Saving support
    func saveMainThreadContext() {
        try? saveContext(mainThreadContext!)
    }
    
    func saveContext(_ moc: NSManagedObjectContext, error: NSErrorPointer? = nil) {
        if moc.hasChanges {
            do {
                try moc.save()
            } catch let error1 as NSError {
                error??.pointee = error1
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                NSLog("Unresolved error \(error??.pointee), \(error??.pointee?.userInfo)")
                abort()
            }
        }
    }

    
}

