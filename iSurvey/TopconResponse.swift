//
//  TopconGTSProtocol.swift
//  iSurvey
//
//  Created by Chris Morse on 2/4/15.
//  Copyright (c) 2015 Morse Technologies, Inc. All rights reserved.
//

import Foundation

// MARK: - Response packet structure
struct ResponsePacket: CustomStringConvertible, CustomDebugStringConvertible {
    var rawString: String
    
    //Header
    var responseType: ResponseType?
    var responseID: CChar?
    
    //Distances
    var horizontalDistance: Double?
    var verticalDistance: Double?
    var slopeDistance: Double?
    var stakeOutDistance: Double?
    var angleType: AngleType?

    //Coordinates
    var northing: Double?
    var easting: Double?
    var elevation: Double?
    
    //Angles
    var horizontalAngle: Angle?
    var verticalAngle: Angle?
    
    //Instrument details
    var tiltCorrection: TiltCorrection?
    var edmSignalLevel: Int?
    var edmPPMCorrection: Int?
    var prismConstant: Int?
    
    var instrumentHeight: Double?
    var prismHeight: Double?
    
    //Units
    var distanceUnits: DistanceUnits?
    var angleUnits: AngleUnits?
    
    //Footer
    var bcc: String?
    var terminator: String?
    
    //Derived values
    var isValid: Bool = false   //When true send an ACK request; if false, send a NAK
    
    ///Parse a ResponsePacket from the Raw Response Data.
    ///(Raw data must already be parsed into a single complete packet that retains the ETX CRLF terminator)
    init(rawResponse: String) {
        rawString = rawResponse
        
        if (rawString.characters.count >= 7) { //7 is the smallest valid string: (1)commandType, (3)bcc, (1)ETX, (2)CRLF
            self.responseType = ResponseType(string: rawString)

            //Use the mapped parser to do the rest
            if let success = self.responseType?.parser(&self) {
                isValid = success
            }
        }
    }
    
    typealias KeyValueTuple = (String, Any)
    func values() -> [KeyValueTuple] {
        var array:[KeyValueTuple] = Array()
        
        if (responseID != nil) {
            let x: KeyValueTuple = ("ID", String(format: "%c", responseID!))
            array.append(x) }
        //Angles
        if (verticalAngle != nil) {
            let x: KeyValueTuple = ("Vº", verticalAngle!); array.append(x) }
        if (horizontalAngle != nil) {
            let x: KeyValueTuple = ("Hº", horizontalAngle!); array.append(x) }
        //Distances
        if (horizontalDistance != nil)  {
            let x: KeyValueTuple = ("HD", horizontalDistance!); array.append(x) }
        if (verticalDistance != nil)    {
            let x: KeyValueTuple = ("VD", verticalDistance!); array.append(x) }
        if (slopeDistance != nil)       {
            let x: KeyValueTuple = ("SD", slopeDistance!); array.append(x) }
        if (stakeOutDistance != nil)    {
            let x: KeyValueTuple = ("SO", stakeOutDistance!); array.append(x) }
        //Coordinates
        if (northing != nil)            {
            let x: KeyValueTuple = (" N", northing!); array.append(x) }
        if (easting != nil)             {
            let x: KeyValueTuple = (" E", easting!); array.append(x) }
        if (elevation != nil)           {
            let x: KeyValueTuple = (" Z", elevation!); array.append(x) }
        //Instrumentation
        if (tiltCorrection != nil)      {
            let x: KeyValueTuple = (" t", tiltCorrection!); array.append(x) }
        if (edmSignalLevel != nil)      {
            let x: KeyValueTuple = (" L", edmSignalLevel!); array.append(x) }
        if (edmPPMCorrection != nil)    {
            let x: KeyValueTuple = (" P", edmPPMCorrection!); array.append(x) }
        if (prismConstant != nil)       {
            let x: KeyValueTuple = (" O", prismConstant!); array.append(x) }
        if (instrumentHeight != nil)    {
            let x: KeyValueTuple = ("IH", instrumentHeight!); array.append(x) }
        if (prismHeight != nil)         {
            let x: KeyValueTuple = ("PH", prismHeight!); array.append(x) }

        return array
    }
    
    var description: String {
        var desc = (responseID != nil)      ? String(format: "ID:%c", responseID!) : ""
        //Angles
        desc += (horizontalAngle != nil)    ? " Hº:\(horizontalAngle!)" : ""
        desc += (verticalAngle != nil)      ? " Vº:\(verticalAngle!)" : ""
        //Distances
        desc += (horizontalDistance != nil) ? " HD:\(horizontalDistance!)" : ""
        desc += (verticalDistance != nil)   ? " VD:\(verticalDistance!)" : ""
        desc += (slopeDistance != nil)      ? " SD:\(slopeDistance!)" : ""
        desc += (stakeOutDistance != nil)   ? " SD:\(stakeOutDistance!)" : ""
        //Coordinates
        desc += (northing != nil)           ? "  N:\(northing!)" : ""
        desc += (easting != nil)            ? "  E:\(easting!)" : ""
        desc += (elevation != nil)          ? "  Z:\(elevation!)" : ""
        //Instrumentation
        desc += (tiltCorrection != nil)     ? "  t:\(tiltCorrection!)" : ""
        desc += (edmSignalLevel != nil)     ? "  L:\(edmSignalLevel!)" : ""
        desc += (edmPPMCorrection != nil)   ? "  P:\(edmPPMCorrection!)" : ""
        desc += (prismConstant != nil)      ? "  O:\(prismConstant!)" : ""
        desc += (instrumentHeight != nil)   ? " IH:\(instrumentHeight!)" : ""
        desc += (prismHeight != nil)        ? " PH:\(prismHeight!)" : ""
        
        return desc
    }
    
    var debugDescription: String {
        return description
    }
    
    var prettyPrintedDescription: String {
        var desc = "{\n"
        for (key, value) in values() {
            desc += " \(key): \(value)\n"
        }
        desc += "}"
        return desc
    }
}


// MARK: - Response Types
enum ResponseType: CChar {
    
    ///Unknown type - used for any response that doesn't match a known type
    case unknown = 0x00
    
    ///ACK (ID: 06H)
    case ack = 0x06 //"␆"
    
    ///NAK (ID: 21H)
    case nak = 0x21 //"␕"
    
    ///Recall data (ID: 4CH)
    case recallData = 0x4C //"L"
    
    ///SD mode (ID: 3FH)
    case sdMode = 0x3F //"?"
    
    ///HD or VD mode (ID: 52H)
    case hDorVDMode = 0x52 //"R"

    ///V or H angle mode (ID: 3CH)
    case vhAngleMode = 0x3C //"<"
    
    ///N or E or Z coordinates mode - <Standard Mode> (ID: 55H)
    case nezCoordinatesStandardMode = 0x55 //"U"
    case enzCoordinatesStandardMode = 0x2F //"/"
    
    ///N or E or Z coordinates mode - <Expanded mode> (ID:57H)
    case nezCoordinatesExpandedMode = 0x57 //"W"
    
    ///SD TRK(10mm) (SD T.R) mode (ID: 44H)
    case sdTrackMode = 0x44 //"D"
    
    ///HD TRK(10mm) (HD T.R) mode (ID: 41H)
    case hdTrackMode = 0x41 //"A"
    
    ///VD TRK(10mm) (VD T.R) mode (ID: 45H)
    case vdTrackMode = 0x45 //"E"
    
    var sampleResponse: String {
        switch self {
        case .ack:          return "\u{6}006\u{3}\r\n"
        case .nak:          return "\u{21}021\u{3}\r\n"
        case .recallData:
            return "L+00000000-0000046100+0000082350-0000000250f+002220000fv+0010110+0020220f103\u{3}\r\n"
        case .sdMode:
            return "?+00006145f0060612+1701650d+00006110t60+06+00040rt087\u{3}\r\n"
        case .hDorVDMode:
            return "R+00006115f0060647+1701524d+00000655t**+06+00010rt060\u{3}\r\n"
        case .vhAngleMode:
            return "<0060603+1701512-0037d000rt110\u{3}\r\n"
        case .nezCoordinatesStandardMode:
            return "U-00010630+00009265+00000630f+1701651d040rt095\u{3}\r\n"
        case .enzCoordinatesStandardMode:
            return "/-0000106320+0000092660+0000006280f+170165000030rt079\u{3}\r\n"
        case .nezCoordinatesExpandedMode:
            return "W+000061400f00606060+17016500d-0000106300+0000092650+0000006300t60+0060+000040rt054\u{3}\r\n"
        case .sdTrackMode:
            return "D+00006140f020rt062\u{3}\r\n"
        case .hdTrackMode:
            return "A+00006100f020rt063\u{3}\r\n"
        default:            return "\u{6}006\r\n"
        }
    }
    

    var parser: ResponseParser {
        switch self {
        case .ack:                          return ParseAckNak
        case .nak:                          return ParseAckNak
        case .recallData:                   return Parse4C
        case .sdMode:                       return Parse3F
        case .hDorVDMode:                   return Parse52
        case .vhAngleMode:                  return Parse3C
        case .nezCoordinatesStandardMode:   return Parse55
        case .nezCoordinatesExpandedMode:   return Parse57
        case .sdTrackMode:                  return Parse44
        case .hdTrackMode:                  return Parse41
        case .vdTrackMode:                  return Parse45
        default:                            return ParseNotImplemented
        }
    }
    
    init?(string: String) {

        if let cstring = string.cString(using: .utf8) {
            let char = cstring[0]
            if let type = ResponseType(rawValue: char) {
                self = type
                return
            }
        }
        return nil
    }
}

///Takes the response sentence and parses it using the given regex `pattern`. 
///
/// Returns: array of string tokens
func tokenizeResponse(_ response: String, pattern: String) -> [String]? {
    
    do {
        var tokens = [String]()
        let range = NSMakeRange(0, response.characters.count)
        let regex = try NSRegularExpression(pattern: pattern)
        debugPrint("regex: \(regex)")

        regex.enumerateMatches(in: response, range: range, using: { (result, flags, stop) -> Void in
            if let tokenCount = result?.numberOfRanges {
                for i in 1 ..< tokenCount {
                    let token = (response as NSString).substring(with: result!.rangeAt(i))
                    tokens.append(token)
                }
            }
        })

        return tokens
    } catch let error {
        print("Exception creating regular expression with pattern: \(pattern) \n Exception: \(error)")
        return nil
    }
}


//MARK: - Response Parsers
typealias ResponseParser = (inout ResponsePacket) -> Bool

//MARK: Generic parser for unimplemented responses
///Generic parser for unimplemented responses. Always fails
let ParseNotImplemented: ResponseParser = { (p) -> Bool in return false }

//MARK: ACK/NAK (ID: 06H)
///ACK/NAK (ID: 06H)
let ParseAckNak: ResponseParser = { (p) -> Bool in
    //Ex: ␆  006 ETX CRLF
    //Ex: ␕  021 ETX CRLF
    //    |  |
    //    ID BCC

    // Documented format
    //0         1         2         3         4         5         6         7         8
    //012345678901234567890123456789012345678901234567890123456789012345678901234567890
    // B  †
    //␆006␃␍␊
    //␕021␃␍␊

    let bytes = p.rawString
    let responseIdIdx = bytes.index(bytes.startIndex, offsetBy: 1)
    let bccIdx = bytes.index(responseIdIdx, offsetBy: 3)
    let terminatorIdx = bytes.index(bccIdx, offsetBy: 3)

    p.responseID       = p.responseType!.rawValue
    p.bcc              = bytes[responseIdIdx...bccIdx]
    p.terminator       = bytes[bccIdx...terminatorIdx]
    return true
}

//MARK: Recall Data Parser (ID: 4CH)
///Recall Data Parser (ID: 4CH)
let Parse4C: ResponseParser = { (p) -> Bool in
    //Ex: L  +12030400d     +1000000000+2000000000+0000300000m   +002000000 m   h
    //    |  |        |     |          |          |          |   |          |   |
    //    ID H        d/g/m N          E          Z          m/f D          m/f s/h/v
    //
    //Continued...
    //    +0015000+0020000 m   054 ETX CRLF
    //    |       |        |   |
    //    Inst.H  P.H      m/f BCC
    //
    //H:        Horizontal angle         120°30’40.0”   (+/-  8 digits)
    //N:        N coordinate            +100000.0000m   (+/- 10 digits)
    //E:        E coordinate            +200000.0000m   (+/- 10 digits)
    //Z:        Z coordinate                +30.0000m   (+/- 10 digits)
    //D:        Stake out distance         +200.0000m   (+/-  9 digits)
    //s/h/v:    Stake out distance type             s   (s: slope, h: horizontal, v: vertical)
    //Inst.H:   Instrument height            +1.5000m   (+/-  7 digits)
    //P.H:      Prism height                 +2.0000m   (+/-  7 digits)
    //
    // Documented format
    //L+12030400d+1000000000+2000000000+0000300000m+002000000mh+0015000+0020000m054␃␍␊
    // Hº       aN          E          Z          dD         dhI       P       mB  †
    //
    // ACTUAL format returned by GTS-802A
    //L+00000000-0000046100+0000082350-0000000250f+002220000fv+0010110+0020220f103␃␍␊
    // Hº       N      .   E      .   Z      .   dD     .   dhI   .   P   .   mB  †

    let pattern = Patterns.ID   // 0: ResponseType
        + Patterns.Angle        // 1: HAngle
        //                      // 2: (missing units token)
        + Patterns.Distance     // 3: N
        //                      // 4: (missing units token)
        + Patterns.Distance     // 5: E
        //                      // 6: (missing units token)
        + Patterns.Distance     // 7: Z
        //                      // 8: distance units
        + Patterns.Distance     // 9: SO - Stake Out Distance
        //                      //10: distance units
        + Patterns.AngleType    //11: SO distance type (slope/horizontal/vertical)
        + Patterns.Distance     //12: I - Instrument Height
        //                      //13: (missing units token)
        + Patterns.Distance     //14: P - Prism Height
        //                      //15: distance units
        + Patterns.BCC          //16: BCC
        + Patterns.Terminator   //17: ␃␍␊
    
    if var tokens = tokenizeResponse(p.rawString, pattern: pattern) {
        if (tokens.count != 18) { return false }
        p.responseID        = CChar(Int(tokens[0]) ?? 0)
        p.horizontalAngle   = Angle(string: tokens[1])
        p.northing          = tokens[3].toDouble(4)
        p.easting           = tokens[5].toDouble(4)
        p.elevation         = tokens[7].toDouble(4)
        p.stakeOutDistance  = tokens[9].toDouble(4)
        p.angleType         = AngleType(rawValue: tokens[11])
        p.instrumentHeight  = tokens[12].toDouble(4)
        p.prismHeight       = tokens[14].toDouble(4)
        p.bcc              = tokens[18]
        p.terminator       = tokens[19]
        return true
    }
    return false
}

// MARK: SD mode Parser (ID: 3FH)
///SD mode Parser (ID: 3FH)
let Parse3F: ResponseParser = { (p) -> Bool in
    //Ex: ?  +01178481m   0852030+1203040d     +01174572t   15 +00 +25 099 ETX CRLF
    //    |  |        |   |      |       |     |        |   |  |   |   |
    //    ID SD       m/f V      H       d/g/m HD       t/* L  P   O   BCC
    //
    //￼￼ID:       An identification character which discriminates the kind of data (ASCII 3FH)
    //SD:       Slope distance             1178.481m    (+/-, 8 digits)
    //m/f:      meter/feet
    //V:        Vertical angle             85°20’30”         (7 digits)
    //H:        Horizontal angle          120°30’40”    (+/-, 7 digits)
    //          ( +/- : angle turned to right (+)
    //                  or left (-) )
    //d/g/m:    degree/grad/mil
    //HD:       Horizontal distance        1174.572m    (+/-, 7 digits)
    //t/*:      Tilt correction -- t: yes,*: no
    //L:        EDM return signal level          15          (2 digits)
    //P:        EDM atmospheric correction      +00 pp  (+/-, 2 digits)
    //O:        Prism constant                  +25mm   (+/-, 2 digits)
    //BCC:      Block check character           099          (3 digits)
    //
    //Note 1) The distance data is represented in the unit of 0.001m or 0.001ft.
    //        The data length is fixed 8 digits.
    //
    //Note 2) The angle data is represented in from of xxx°xx’xx” or in the form of xxx.xxxx gon or xxxx.xxx mil.
    //        The data length is fixed 7 digits.
    //
    //Note 3) In case of coarse mode, the signal level is replaced by ”** ”.
 
    // Documented format
    //?+01178481m0852030+1203040d+01174572t15+00+25099␃␍␊
    // SD       mVº     Hº      dHD       tL P  O  B  †
    
    // ACTUAL format returned by GTS-802A
    //?+00006145f0060612+1701650d+00006110t60+06+00040rt087␃␍␊
    // SD       mVº     Hº      dHD       tL P  O  ?    B  †
    let pattern = Patterns.ID   // 0: ResponseType
        + Patterns.Distance     // 1: SD
        //                      // 2: distance units
        + Patterns.Angle        // 3: VAngle
        //                      // 4: (missing units token)
        + Patterns.Angle        // 5: HAngle
        //                      // 6: angle units
        + Patterns.Distance     // 7: HD
        //                      // 8: (missing units token)
        + Patterns.Tilt         // 9: Tilt
        + Patterns.EDMSignal    //10: EDM signal level
        + Patterns.EDMppm       //11: EDM PPM
        + Patterns.Prism        //12: Prism constant
        + Patterns.Unknown      //13: Unknown data
        + Patterns.BCC          //14: BCC
        + Patterns.Terminator   //15: ␃␍␊
    
    if var tokens = tokenizeResponse(p.rawString, pattern: pattern) {
        if (tokens.count != 16) { return false }
        p.responseID        = CChar(Int(tokens[0]) ?? 0)
        p.slopeDistance     = tokens[1].toDouble(3)
        p.distanceUnits     = DistanceUnits(rawValue: tokens[2])
        p.verticalAngle     = Angle(string: tokens[3])
        p.horizontalAngle   = Angle(string: tokens[5])
        p.angleUnits        = AngleUnits(rawValue: tokens[6])
        p.horizontalDistance = tokens[7].toDouble(3)
        p.tiltCorrection    = TiltCorrection(rawValue: tokens[9])
        p.edmSignalLevel    = Int(tokens[10])
        p.edmPPMCorrection  = Int(tokens[11])
        p.prismConstant     = Int(tokens[12])
        //?: No idea what this is
        p.bcc              = tokens[14]
        p.terminator       = tokens[15]
        return true
    }
    return false //parsing failed
}

// MARK: SD (0.2mm) mode - Parse28
///SD (0.2mm) mode Parser (ID: 28H)
let Parse28: ResponseParser = { (p) -> Bool in
    //Ex: (  +011784812m   08520300 +12030400d     +011745724t   15 +0000 +025** 066 ETX CRLF
    //    |  |         |   |        |        |     |         |   |  |    |    |  |
    //    ID SD        m/f Vº       Hº       d/g/m HD        t/* L  P    O    b  BCC
    //
    //￼￼ID:       An identification character which discriminates the kind of data (ASCII 3FH)
    //SD:       Slope distance             1178.481m    (+/-, 9 digits)
    //m/f:      meter/feet
    //Vº:       Vertical angle             85°20’30”         (8 digits)
    //Hº:       Horizontal angle          120°30’40”    (+/-, 8 digits)
    //          ( +/- : angle turned to right (+)
    //                  or left (-) )
    //d/g/m:    degree/grad/mil
    //HD:       Horizontal distance        1174.572m    (+/-, 9 digits)
    //t/*:      Tilt correction -- t: yes,*: no
    //L:        EDM return signal level          15          (2 digits)
    //P:        EDM atmospheric correction      +00 pp  (+/-, 4 digits)
    //O:        Prism constant                  +25mm   (+/-, 2 digits)
    //b:        Unused                           **          (2 digits)
    //BCC:      Block check character           099          (3 digits)
    //
    //Note 1) The distance data is represented in the unit of 0.001m or 0.001ft.
    //        The data length is fixed 8 digits.
    //
    //Note 2) The angle data is represented in from of xxx°xx’xx” or in the form of xxx.xxxx gon or xxxx.xxx mil.
    //        The data length is fixed 7 digits.
    //
    //Note 3) In case of coarse mode, the signal level is replaced by ”** ”.
    
    // Documented format
    //(+011784812m08520300+12030400d+011745724t15+0000+025**066␃␍␊
    // SD        mVº      Hº       dHD        tL P    O   b B  †
    
    // ACTUAL format returned by GTS-802A
    //(+000061440f00605500+170164601+000061090t60+0060+000**030rt041␃␍␊
    // SD        mVº      Hº        HD        tL P    O   b ?    B  †
    let pattern = Patterns.ID   // 0: ResponseType
        + Patterns.Distance     // 1: SD
                                // 2: (possible units token)
        + Patterns.Angle        // 3: VAngle
                                // 4: (possible units token)
        + Patterns.Angle        // 5: HAngle
                                // 6: (possible units token)
        + Patterns.Distance     // 7: HD
                                // 8: (possible units token)
        + Patterns.Tilt         // 9: Tilt
        + Patterns.EDMSignal    //10: EDM signal level
        + Patterns.EDMppm       //11: EDM PPM
        + Patterns.Prism        //12: Prism constant
        + Patterns.Unused       //13: Unused
        + Patterns.Unknown      //14: Unknown data
        + Patterns.BCC          //15: BCC
        + Patterns.Terminator   //16: ␃␍␊
    
    if var tokens = tokenizeResponse(p.rawString, pattern: pattern) {
        if (tokens.count != 16) { return false }
        p.responseID        = CChar(Int(tokens[0]) ?? 0)
        p.slopeDistance     = tokens[1].toDouble(4)
        p.distanceUnits     = DistanceUnits(rawValue: tokens[2])
        p.verticalAngle     = Angle(string: tokens[3])
        p.horizontalAngle   = Angle(string: tokens[5])
        p.angleUnits        = AngleUnits(rawValue: tokens[6])
        p.horizontalDistance = tokens[7].toDouble(4)
        p.tiltCorrection    = TiltCorrection(rawValue: tokens[9])
        p.edmSignalLevel    = Int(tokens[10])
        p.edmPPMCorrection  = Int(tokens[11])
        p.prismConstant     = Int(tokens[12])
        //b: unused
        //?: No idea what this is
        p.bcc              = tokens[14]
        p.terminator       = tokens[16]
        return true
    }
    return false //parsing failed
}

// MARK: HD or VD mode Parser (ID: 52H)
///HD or VD mode Parser (ID: 52H)
let Parse52: ResponseParser = { (p) -> Bool in
    //Ex: R  +01174572m   0852030+1203040d     +00095802t   15+00+25 084 ETX CRLF
    //    |  |        |   |      |       |     |        |   | |  |   |
    //    ID HD       m/f V      H       d/g/m VD       t/* L P  O   BCC
    //
    //VD:       Vertical distance            95.802m    (+/-, 8 digits)
    //
    // Documented format
    //R+01174572m0852030+1203040d+00095802t15+00+25084␃␍␊
    // HD    .  mVº     Hº      dVD    .  tL P  O  B  †
    //
    // ACTUAL format returned by GTS-802A
    //R+00006115f0060647+1701524d+00000655t**+06+00010rt060␃␍␊
    // HD    .  mVº     Hº      dVD    .  tL P  O  ?    B  †
    
    let pattern = Patterns.ID   // 0: ResponseType
        + Patterns.Distance     // 1: HD
        //                      // 2: distance units
        + Patterns.Angle        // 3: VAngle
        //                      // 4: (missing units token)
        + Patterns.Angle        // 5: HAngle
        //                      // 6: angle units
        + Patterns.Distance     // 7: VD
        //                      // 8: (missing units token)
        + Patterns.Tilt         // 9: Tilt
        + Patterns.EDMSignal    //10: EDM signal level
        + Patterns.EDMppm       //11: EDM PPM
        + Patterns.Prism        //12: Prism constant
        + Patterns.Unknown      //13: Unknown data
        + Patterns.BCC          //14: BCC
        + Patterns.Terminator   //15: ␃␍␊
    
    if var tokens = tokenizeResponse(p.rawString, pattern: pattern) {
        if (tokens.count != 16) { return false }
        p.responseID        = CChar(Int(tokens[0]) ?? 0)
        p.horizontalDistance = tokens[1].toDouble(3)
        p.distanceUnits     = DistanceUnits(rawValue: tokens[2])
        p.verticalAngle     = Angle(string: tokens[3])
        p.horizontalAngle   = Angle(string: tokens[5])
        p.angleUnits        = AngleUnits(rawValue: tokens[6])
        p.verticalDistance  = tokens[7].toDouble(3)
        p.tiltCorrection    = TiltCorrection(rawValue: tokens[9])
        p.edmSignalLevel    = Int(tokens[10])
        p.edmPPMCorrection  = Int(tokens[11])
        p.prismConstant     = Int(tokens[12])
        //?: No idea what this is
        p.bcc               = tokens[14]
        p.terminator        = tokens[15]
        return true
    }
    return false
}

// MARK: V or H angle mode Parser (ID: 3CH)
///V or H angle mode Parser (ID: 3CH)
let Parse3C: ResponseParser = { (p) -> Bool in
    //Ex: <  0862405 +1745545 +0127  d     082 ETX (CRLF)
    //    |  |       |        |      |     |
    //    ID Vº      Hº       X.TILT d/g/m BCC
    //
    //X.TILT:   Tilt correction of vertical +1’27”  (+/-, 4 digits)
    //
    //Note 1) The tilt angle is represented as xx’xx” or 0.xxxx gon or x.xxx mil.
    //Note 2) The tilt data would be replaced by ”**** ” if the correction is stopped by the mode setting.
    //
    // Documented format
    //<0862405+1745545+0127d082␃␍␊
    // Vº     Hº      X    dB  †
    //
    // ACTUAL format returned by GTS-802A
    //<0060603+1701512-0037d000rt110␃␍␊
    // Vº     Hº      X    dB  ?    †
    
    let pattern = Patterns.ID   // 0: ResponseType
        + Patterns.Angle        // 1: VAngle
        //                      // 2: (missing units token)
        + Patterns.Angle        // 3: HAngle
        //                      // 4: (missing units token)
        + Patterns.XTilt        // 5: X - X.Tilt
        + Patterns.AngleUnit    // 6: angle units
        + Patterns.Unknown      // 7: Unknown data
        + Patterns.BCC          // 8: BCC
        + Patterns.Terminator   // 9: ␃␍␊
    
    if var tokens = tokenizeResponse(p.rawString, pattern: pattern) {
        if (tokens.count != 10) { return false }
        p.responseID        = CChar(Int(tokens[0]) ?? 0)
        p.verticalAngle     = Angle(string: tokens[1])
        p.horizontalAngle   = Angle(string: tokens[3])
        //Don't support X.Tilt yet
        p.angleUnits        = AngleUnits(rawValue: tokens[6])
        //?: No idea what this is
        p.bcc               = tokens[7]
        p.terminator        = tokens[8]
        return true
    }
    return false
}


// MARK: N or E or Z coordinates mode - <Standard Mode> Parser (ID: 55H)
///N or E or Z coordinates mode - <Standard Mode> Parser (ID: 55H)
let Parse55: ResponseParser = { (p) -> Bool in
    //Ex: U  -00596337+01011930+00095802m   +1203040d     110 ETX CRLF
    //    |  |        |        |        |   |       |     |
    //    ID N        E        Z        m/f H       d/g/m BCC
    //N:        N coordinate               -596.337m    (+/-, 8 digits)
    //E:        E coordinate              +1011.930m    (+/-, 8 digits)
    //Z:        Z coordinate                +95.802m    (+/-, 8 digits)
    //
    // Documented format
    //U-00596337+01011930+00095802m+1203040d110␃␍␊
    // N     .  E     .  Z     .  mHº      dB  †
    //
    // ACTUAL format returned by GTS-802A
    //U-00010630+00009265+00000630f+1701651d040rt095␃␍␊
    // N     .  E     .  Z     .  mHº      d?    B  †
    
    let pattern = Patterns.ID   // 0: ResponseType
        + Patterns.Distance     // 1: N
        //                      // 2: (missing units token)
        + Patterns.Distance     // 3: E
        //                      // 4: (missing units token)
        + Patterns.Distance     // 5: Z
        //                      // 6: distance units
        + Patterns.Angle        // 7: HAngle
        //                      // 8: angle units
        + Patterns.Unknown      // 9: Unknown data
        + Patterns.BCC          //10: BCC
        + Patterns.Terminator   //11: ␃␍␊
    
    if var tokens = tokenizeResponse(p.rawString, pattern: pattern) {
        if (tokens.count != 12) { return false }
        p.responseID        = CChar(Int(tokens[0]) ?? 0)
        p.northing          = tokens[1].toDouble(4)
        p.easting           = tokens[3].toDouble(4)
        p.elevation         = tokens[5].toDouble(4)
        p.distanceUnits     = DistanceUnits(rawValue: tokens[6])
        p.horizontalAngle   = Angle(string: tokens[7])
        p.angleUnits        = AngleUnits(rawValue: tokens[8])
        //?: No idea what this is
        p.bcc              = tokens[10]
        p.terminator       = tokens[11]
        return true
    }
    return false
}

// MARK: N or E or Z coordinates mode - <Expanded mode> Parser (ID:57H)
///N or E or Z coordinates mode - <Expanded mode> Parser (ID:57H)
let Parse57: ResponseParser = { (p) -> Bool in
    //Ex: W  +011784810m   0852000+12030400d
    //    |  |         |   |      |        |
    //    ID SD        m/f V      H        d/g/m
    //
    //Continued:
    //    -0005963370+0010119300+0000958020t   15+0000+250 014 ETX CRLF
    //    |          |          |          |   | |    |    |
    //    N          E          Z          t/* L P    O    BCC
    //
    //Note 1) The coordinate data is represented in the unit of 0.0001m or 0.0001ft.
    //        The data length is fixed 10 digits.
    //
    //Note 2) If the coordinate data will be more than +1,000,000.0000m/ft, the number will
    //        be outputted at the location of sign.
    //

    // Documented format
    //W+011784810m0852000+12030400d-0005963370+0010119300+0000958020t15+0000+250014␃␍␊
    // SD        mVº     Hº       dN          E          Z          tL P    O   B  †
    
    // ACTUAL format returned by GTS-802A
    //W+000061400f00606060+17016500d-0000106300+0000092650+0000006300t60+0060+000040rt054␃␍␊
    // SD    .   mVº      Hº       dN      .   E      .   Z      .   tL P    O   ?    B  †
    
    let pattern = Patterns.ID   // 0: ResponseType
        + Patterns.Distance     // 1: SD
        //                      // 2: distance units
        + Patterns.Angle        // 3: VAngle
        //                      // 4: (missing units token)
        + Patterns.Angle        // 5: HAngle
        //                      // 6: angle units
        + Patterns.Distance     // 7: N
        //                      // 8: (missing units token)
        + Patterns.Distance     // 9: E
        //                      //10: (missing units token)
        + Patterns.Distance     //11: Z
        //                      //12: (missing units token)
        + Patterns.Tilt         //13: Tilt
        + Patterns.EDMSignal    //14: EDM signal level
        + Patterns.EDMppm       //15: EDM PPM
        + Patterns.Prism        //16: Prism constant
        + Patterns.Unknown      //17: Unknown data
        + Patterns.BCC          //18: BCC
        + Patterns.Terminator   //19: ␃␍␊
    
    if var tokens = tokenizeResponse(p.rawString, pattern: pattern) {
        if (tokens.count != 20) { return false }
        p.responseID        = CChar(Int(tokens[0]) ?? 0)
        p.slopeDistance     = tokens[1].toDouble(4)
        p.distanceUnits     = DistanceUnits(rawValue: tokens[2])
        p.verticalAngle     = Angle(string: tokens[3])
        p.horizontalAngle   = Angle(string: tokens[5])
        p.angleUnits        = AngleUnits(rawValue: tokens[6])
        p.northing          = tokens[7].toDouble(4)
        p.easting           = tokens[9].toDouble(4)
        p.elevation         = tokens[11].toDouble(4)
        p.tiltCorrection    = TiltCorrection(rawValue: tokens[13])
        p.edmSignalLevel    = Int(tokens[14])
        p.edmPPMCorrection  = Int(tokens[15])
        p.prismConstant     = Int(tokens[16])
        //?: No idea what this is
        p.bcc              = tokens[18]
        p.terminator       = tokens[19]
        return true
    }
    return false
}


// MARK: SD TRK(10mm) (SD T.R) mode Parser (ID: 44H)
///SD TRK(10mm) (SD T.R) mode Parser (ID: 44H)
let Parse44: ResponseParser = { (p) -> Bool in
    //Ex: D  +01178480 m   001 ETX CRLF
    //    |  |         |   |
    //    ID SD        m/f BCC
    //
    // Documented format
    //D+01178480m001␃␍␊
    // SD    .  mB  †
    //
    // ACTUAL format returned by GTS-802A
    //D+00006140f020rt062␃␍␊
    // SD    .  m?    B  †
    
    let pattern = Patterns.ID   // 0: ResponseType
        + Patterns.Distance     // 1: N
        //                      // 2: distance units
        + Patterns.Unknown      // 3: Unknown data
        + Patterns.BCC          // 4: BCC
        + Patterns.Terminator   // 5: ␃␍␊
    
    if var tokens = tokenizeResponse(p.rawString, pattern: pattern) {
        if (tokens.count != 6) { return false }
        p.responseID        = CChar(Int(tokens[0]) ?? 0)
        p.slopeDistance     = tokens[1].toDouble(3)
        p.distanceUnits     = DistanceUnits(rawValue: tokens[2])
        //?: No idea what this is
        p.bcc              = tokens[4]
        p.terminator       = tokens[5]
        return true
    }
    return false
}

// MARK: HD TRK(10mm) (HD T.R) mode Parser (ID: 41H)
///HD TRK(10mm) (HD T.R) mode Parser (ID: 41H)
let Parse41: ResponseParser = { (p) -> Bool in
    //Ex: A  +01174570 m   006 ETX CRLF
    //    |  |         |   |
    //    ID HD        m/f BCC
    //
    // Documented format
    //A+01178480m001␃␍␊
    // HD    .  mB  †
    //
    // ACTUAL format returned by GTS-802A
    //A+00006100f020rt063␃␍␊
    // HD    .  m?    B  †
    
    let pattern = Patterns.ID   // 0: ResponseType
        + Patterns.Distance     // 1: N
        //                      // 2: distance units
        + Patterns.Unknown      // 3: Unknown data
        + Patterns.BCC          // 4: BCC
        + Patterns.Terminator   // 5: ␃␍␊
    
    if var tokens = tokenizeResponse(p.rawString, pattern: pattern) {
        if (tokens.count != 6) { return false }
        p.responseID        = CChar(Int(tokens[0]) ?? 0)
        p.horizontalDistance = tokens[1].toDouble(3)
        p.distanceUnits     = DistanceUnits(rawValue: tokens[2])
        //?: No idea what this is
        p.bcc              = tokens[4]
        p.terminator       = tokens[5]
        return true
    }
    return false
}

// MARK: VD TRK(10mm) (VD T.R) mode Parser (ID: 45H)
///VD TRK(10mm) (VD T.R) mode Parser (ID: 45H)
let Parse45: ResponseParser = { (p) -> Bool in
    //Ex: E  +00095800 m   007 ETX CRLF
    //    |  |         |   |
    //    ID VD        m/f BCC
    //
    // Documented format
    //E+01178480m001␃␍␊
    // VD    .  mB  †
    //
    // ACTUAL format returned by GTS-802A
    //???
    // SD    .  m?    B  †
    
    let pattern = Patterns.ID   // 0: ResponseType
        + Patterns.Distance     // 1: N
        //                      // 2: distance units
        + Patterns.Unknown      // 3: Unknown data
        + Patterns.BCC          // 4: BCC
        + Patterns.Terminator   // 5: ␃␍␊
    
    if var tokens = tokenizeResponse(p.rawString, pattern: pattern) {
        if (tokens.count != 6) { return false }
        p.responseID        = CChar(Int(tokens[0]) ?? 0)
        p.verticalDistance  = tokens[1].toDouble(3)
        p.distanceUnits     = DistanceUnits(rawValue: tokens[2])
        //?: No idea what this is
        p.bcc              = tokens[4]
        p.terminator       = tokens[5]
        return true
    }
    return false
}

