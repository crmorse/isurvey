
import UIKit
import CoreData
import MapKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
private func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
private func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class StationDetailsVC: UIViewController, UITextFieldDelegate, UITextViewDelegate {
    
    var station: Station?
    
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var created: UITextField!
    @IBOutlet weak var location: UITextField!
    @IBOutlet weak var backsight: UITextField!
    @IBOutlet weak var foresight: UITextField!
    @IBOutlet weak var measurements: UITextField!
    
    @IBOutlet weak var notes: UITextView!
    
    @IBOutlet weak var mapVIew: MKMapView!
    
    override func viewDidLoad() {
        name.text = station?.name
        created.text = station?.created?.description
        location.text = station?.location?.description
        backsight.text = station?.backsight.debugDescription
        foresight.text = station?.foresight?.debugDescription
        measurements.text = "\(station?.measurements.count ?? 0)"
        notes.text = station?.memo
        
        if let coord = station?.location?.coordinate {
            mapVIew.centerCoordinate = coord
        } else {
            mapVIew.isHidden = true
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? StationDetailsVC {
            vc.station = sender as? Station
        }
        else if let vc = segue.destination as? ShotsTVC {
            vc.station = sender as? Station
        }
    }
    
    @IBAction func viewBacksight(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "showShotDetails", sender: station?.backsight)
    }
    
    @IBAction func viewForesight(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "showShotDetails", sender: station?.foresight)
    }
    
    @IBAction func viewMeasurements(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "showShots", sender: station)
    }
 
    func textFieldDidEndEditing(_ textField: UITextField) {
        if (textField.text?.lengthOfBytes(using: String.Encoding.utf8) > 0) {
            if station == nil {
                station = NSEntityDescription.insertNewObject(forEntityName: "Station", into: CDM.mainThreadContext) as? Station
                station?.created = Date()
                station?.location = LM.currentLocation
            }
            
            station?.name = name.text
            station?.memo = notes.text
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        
        return true
    }
}
