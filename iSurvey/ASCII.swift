//
//  ASCII.swift
//  iSurvey
//
//  Created by Chris Morse on 2/4/15.
//  Copyright (c) 2015 Morse Technologies, Inc. All rights reserved.
//

import Foundation

enum ASCIIControlCharacter: UInt8 {
    
    //Excerpted from: https://en.wikipedia.org/wiki/ASCII#ASCII_control_characters
    //Dec   HEX Abbr    Print   Caret   Escape  Name
    // 0	00	NUL     ␀       ^@      \0      Null character
    // 1	01	SOH     ␁       ^A              Start of Header
    // 2	02	STX     ␂       ^B              Start of Text
    // 3	03	ETX     ␃       ^C              End of Text
    // 4	04	EOT     ␄       ^D              End of Transmission
    // 5	05	ENQ     ␅       ^E              Enquiry
    // 6	06	ACK     ␆       ^F              Acknowledgment
    // 7	07	BEL     ␇       ^G      \a      Bell
    // 8	08	BS      ␈       ^H      \b      Backspace[d][e]
    // 9	09	HT      ␉       ^I      \t      Horizontal Tab[f]
    //10	0A	LF      ␊       ^J      \n      Line feed
    //11	0B	VT      ␋       ^K      \v      Vertical Tab
    //12	0C	FF      ␌       ^L      \f      Form feed
    //13	0D	CR      ␍       ^M      \r      Carriage return[g]
    //14	0E	SO      ␎       ^N              Shift Out
    //15	0F	SI      ␏       ^O              Shift In
    //16	10	DLE     ␐       ^P              Data Link Escape
    //17	11	DC1     ␑       ^Q              Device Control 1 (oft. XON)
    //18	12	DC2     ␒       ^R              Device Control 2
    //19	13	DC3     ␓       ^S              Device Control 3 (oft. XOFF)
    //20	14	DC4     ␔       ^T              Device Control 4
    //21	15	NAK     ␕       ^U              Negative Acknowledgment
    //22	16	SYN     ␖       ^V              Synchronous idle
    //23	17	ETB     ␗       ^W              End of Transmission Block
    //24	18	CAN     ␘       ^X              Cancel
    //25	19	EM      ␙       ^Y              End of Medium
    //26	1A	SUB     ␚       ^Z              Substitute
    //27	1B	ESC     ␛      ^[       \e[h]	Escape[i]
    //28	1C	FS      ␜       ^\              File Separator
    //29	1D	GS      ␝       ^]              Group Separator
    //30	1E	RS      ␞       ^       ^[j]		Record Separator
    //31	1F	US      ␟       ^_              Unit Separator
    //127	7F	DEL     ␡       ^?              Delete[k][e]

    case nul = 0x00
    case soh
    case stx
    case etx
    case eot
    case enq
    case ack
    case bel
    case bs
    case ht
    case lf
    case vt
    case ff
    case cr
    case so
    case si
    case dle
    case dc1
    case dc2
    case dc3
    case dc4
    case nak
    case syn
    case etb
    case can
    case em
    case sub
    case esc
    case fs
    case gs
    case rs
    case us
    case del = 0x7F
    
    var printable: String {
        switch self {
        case .nul: return "␀"
        case .soh: return "␁"
        case .stx: return "␂"
        case .etx: return "␃"
        case .eot: return "␄"
        case .enq: return "␅"
        case .ack: return "␆"
        case .bel: return "␇"
        case .bs:  return "␈"
        case .ht:  return "␉"
        case .lf:  return "␊"
        case .vt:  return "␋"
        case .ff:  return "␌"
        case .cr:  return "␍"
        case .so:  return "␎"
        case .si:  return "␏"
        case .dle: return "␐"
        case .dc1: return "␑"
        case .dc2: return "␒"
        case .dc3: return "␓"
        case .dc4: return "␔"
        case .nak: return "␕"
        case .syn: return "␖"
        case .etb: return "␗"
        case .can: return "␘"
        case .em:  return "␙"
        case .sub: return "␚"
        case .esc: return "␛"
        case .fs:  return "␜"
        case .gs:  return "␝"
        case .rs:  return "␞"
        case .us:  return "␟"
        case .del: return "␡"
        }
    }
    
    func toString() -> String {
        return String(format: "%c", rawValue)
    }
}
