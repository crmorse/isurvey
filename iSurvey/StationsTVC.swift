import UIKit
import CoreData


class StationsTVC: BaseTVC {
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Station")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "created", ascending: false)]

        self.configureFetchedResultsController(fetchRequest)
        
        do {
            try self.fetchedResultsController.performFetch()
        } catch let error as NSError {
            EM.reportError("Station List", error: error)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? StationDetailsVC {
            vc.station = sender as? Station
        } else if let vc = segue.destination as? ShotsTVC {
            vc.station = sender as? Station
        }
    }
    
    @IBAction func newStation(_ sender: UIBarButtonItem) {
        self.performSegue(withIdentifier: "showStation", sender: nil)
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        switch (editingStyle) {
        case .delete:
            if let station = fetchedResultsController?.object(at: indexPath) as? Station {
                CDM.mainThreadContext.delete(station)
                CDM.saveMainThreadContext()
            }

        default:
            break
        }
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let station = self.fetchedResultsController.object(at: indexPath)
        self.performSegue(withIdentifier: "showShots", sender: station)
    }
    
    override func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        let station = self.fetchedResultsController.object(at: indexPath)
        self.performSegue(withIdentifier: "showStation", sender: station)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StationCell", for: indexPath)
        let station = fetchedResultsController?.object(at: indexPath) as! Station
        
        
        cell.textLabel?.text = station.displayName
        cell.detailTextLabel?.text = station.displayDetails
        
        return cell
    }
}
