//
//  TopconGTSProtocol.swift
//  iSurvey
//
//  Created by Chris Morse on 2/4/15.
//  Copyright (c) 2015 Morse Technologies, Inc. All rights reserved.
//

import Foundation

// MARK: - Request structure
enum RequestType {
    //----- Preset Data Commands -----
    ///Preset the Horizontal Angle
    case presetHAngle(angle: Double, units: AngleUnits)

    ///Preset the Stake out distance
    case presetStakeOut(distance: Double, units: DistanceUnits, measurement: Measurement)
    
    ///Preset the Occupied Station N/E coordinates
    case presetOccupiedStationNECoordinates(northing: Double, easting: Double, units: DistanceUnits)

    ///Preset the Occupied Station Z coordinates
    case presetOccupiedStationZCoordinates(z: Double, units: DistanceUnits)
    
    //----- Recall Data Commands -----
    
    ///This command recall the preset data
    case recallData
    
    
    //----- Measurement Commands -----
    // Type 1: makes the GTS-750 series to start the measurement and to send the measurement data to the computer
    case startMeasuring
    
    // Type 2: answers for the validity of received data
    case ack
    case nak
    
    // Type 3: stops the GTS-750 series sending tracking data (10mm).
    case stopTracking
    
    // Type 4: The GTS-750 series turn on or off the Point Guide when the GTS-750 series receive these commands.
    // (Point Guide type only)
    case pointGuideOn
    case pointGuideOff
    
    // Type 5: changes the measurement mode of the GTS-750 series
    case modeVH
    case modeHR
    case modeHL
    
    case modeSD(mode: DistanceMeasureMode)
    case modeHD(mode: DistanceMeasureMode)
    case modeVD(mode: DistanceMeasureMode)
    case modeNE(mode: DistanceMeasureMode)
    case modeZ (mode: DistanceMeasureMode)

    var commandString: String {
        switch self {

        //----- Preset Data Commands -----
        case .presetHAngle(let angle, let units):
            //Ex: J  +650 d     054 ETX CRLF (ID: 4AH)
            //    |  |    |     |       |
            //    ID H    d/g/m BCC     *Note 1
            //
            //H :Horizontal angle +6’50” (+/-, 1-7 digits)
            //
            //Note 1) The GTS-750 series ignore the CRLF characters.
            //Note 2) The unit of ”d” or ”g” must be the same one which is already set in the GTS-750 series.
            let formattedNumber = angle //TODO: Unit Conversion
            return "J\(formattedNumber)\(units.rawValue)"
            
        case .presetStakeOut(let distance, let units, let measurement):
            //Ex: K  +200000 m   h     103 ETX CRLF (ID: 4BH)
            //    |  |       |   |     |
            //    ID D       m/f h/v/s BCC
            //
            //D: Stake out distance +200.000m (+/-, 1-8 digits)
            //
            //h/v/s -- h: Horizontal distance
            //         v: Vertical distance
            //         s: Slope distance
            //
            //Note) Only one of the stake out distance can be preset at a time.
            let formattedNumber = distance //TODO: Unit Conversion
            return "K\(formattedNumber)\(units.rawValue)\(measurement.rawValue)"
            
        case .presetOccupiedStationNECoordinates(let northing, let easting, let units):
            //Ex: I  +10000000 +20000000 m   039 ETX CRLF (ID: 49H)
            //    |  |         |         |   |
            //    ID N         E         m/f BCC
            //
            //Z coordinates must be preset separately from the N/E coordinates.
            let formattedNorth = northing //TODO: Unit Conversion
            let formattedEast  = easting  //TODO: Unit Conversion
            return "I\(formattedNorth)\(formattedEast)\(units.rawValue)"
            
        case .presetOccupiedStationZCoordinates(let z, let units):
            //Ex: K  -300000 m   z 114 ETX CRLF (ID: 4BH)
            //    |  |       |   | |
            //    ID Z       m/f z BCC
            //
            //Z: Z coordinates 300.000m (-/+, 1-8 digits)
            //z: indicates the preset data is Z coordinates.
            //
            //Note) The sign of the Z coordinates preset data must be reversed because the GTS-750 series
            //      subtract the preset data from the measurement data as in the case of the stake out distance data.
            //
            //      Zm-(-Z)=Zm+Z
            //      Zm: measured Z coordinates with the occupied station coordinates being (0,0,0).
            //      Z: Preset coordinates
            let formattedNumber = z //TODO: Unit Conversion
            return "K\(formattedNumber)\(units.rawValue)z"
            
        //----- Recall Data Commands -----
        case .recallData:        return "LR1"

        //----- Stake Out -----
        case .stopTracking:      return "N"
        case .pointGuideOn:      return "ZA0"
        case .pointGuideOff:     return "ZA1"
            
        //----- Measure Commands -----
        case .startMeasuring:    return "C"
        case .modeHR:            return "Z12"
        case .modeHL:            return "Z13"
        case .modeVH:            return "Z10"
        case .modeSD(let mode):  return "Z3\(mode.rawValue)"
        case .modeHD(let mode):  return "Z4\(mode.rawValue)"
        case .modeVD(let mode):  return "Z5\(mode.rawValue)"
        case .modeNE(let mode):  return "Z6\(mode.rawValue)"
        case .modeZ(let mode):   return "Z8\(mode.rawValue)"
            
        //----- Protocol Commands -----
        case .ack:               return ASCIIControlCharacter.ack.toString()
        case .nak:               return ASCIIControlCharacter.nak.toString()
            
        default:
            print("WARNING: Unexpected command type. Returned ACK")
            return ASCIIControlCharacter.ack.toString()
        }
    }
}

// MARK: - Requst packet builder
struct RequestPacket: CustomStringConvertible, CustomDebugStringConvertible {
    var type: RequestType

    /// Computes the BCC for the entire request.
    /// Precondition: `command` and `resolution` must be already set
    var bcc: String {
        return calculateBCC(type.commandString)
    }

    let terminator = "\(ASCIIControlCharacter.etx.toString())\(ASCIIControlCharacter.cr.toString())\(ASCIIControlCharacter.lf.toString())"
//    let terminator = "\(ASCIIControlCharacter.ETX.printable)\(ASCIIControlCharacter.CR.printable)\(ASCIIControlCharacter.LF.printable)\(ASCIIControlCharacter.CR.toString())\(ASCIIControlCharacter.LF.toString())"
    
    var requestString: String {
        return "\(type.commandString)\(bcc)\(terminator)"
    }
    
    init(type: RequestType) {
        self.type = type
    }
    
    // MARK: - Most Frequent Used requests
    static func ACK() -> RequestPacket {
        return RequestPacket(type: .ack)
    }
    
    static func NAK() -> RequestPacket {
        return RequestPacket(type: .nak)
    }
    
    static func Shoot() -> String {
        return RequestPacket(type: .startMeasuring).requestString
    }
    
    // MARK: Printable protocol
    var description: String {
        return self.requestString
    }

    var debugDescription: String {
        return description
    }

}
