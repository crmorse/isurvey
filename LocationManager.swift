//
//  LocationManager.swift
//  PatriotGroundGame
//
//  Created by Chris Morse on 1/20/15.
//  Copyright (c) 2015 Tea Party Patriots. All rights reserved.
//

import Foundation
import CoreLocation

let LM = LocationManager.sharedInstance

class LocationManager: NSObject, CLLocationManagerDelegate {
    static let sharedInstance = LocationManager()

    private override init() {
        super.init();

        locationManager.delegate = self;

        if (CLLocationManager.locationServicesEnabled()) {
            setTrackingStrategyHighestResolution();
        }
    }

    var locationManager: CLLocationManager = {
        let locMgr = CLLocationManager.init()
        locMgr.requestWhenInUseAuthorization()
        locMgr.requestAlwaysAuthorization()
        return locMgr
    } ()

    func setTrackingStrategyLowestPower() {
        locationManager.startMonitoringSignificantLocationChanges();
        locationManager.stopUpdatingLocation();
    }

    func setTrackingStrategyHighestResolution() {
        locationManager.distanceFilter = kCLDistanceFilterNone;
        locationManager.startUpdatingLocation();
        locationManager.startMonitoringSignificantLocationChanges();
//        locationManager.startMonitoringForRegion(NULL);
    }

    func setTrackingStrategyIntelligentBlend() {
        //Note, requires last location to have successfully reverse-geodecoded or venue information

        /* BEGIN Pseudocode

        I) If current location is relatively static:
        1) Extract location's perimeter based on reverse-geocode or venue database
        2) Set distanceFilter equal to best fitting circumference of perimeter
        3) Setup geo-fence around perimeter

        II) If current location indicates linear travel
        1) Set distanceFilter inversely proportial to speed
                  * i.e. much smaller increments if speed is medium-slow
                  * much larger increments if speed is very fast
                  * Set a floor so that increments are never smaller than 5 minutes

            III) If location history indicates broken travel (i.e. large gaps)
               * Assume phone has limited connectivity; reduce drain as much as possible
               1) startMonitoringSignificantLocationChanges
               2) Turn off high-resolution and geo-fencing
               3) Regularly check for a return to routine tracking

        END Pseudocode */

        //Quick and dirty demo code
        locationManager.distanceFilter = 100;        //medium value of 100 Meters
        locationManager.startUpdatingLocation();
        locationManager.startMonitoringSignificantLocationChanges();
    }

    // MARK: - CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            print("Location Services access granted")
        case .denied, .restricted:
            print("Location Services access DENIED or RESTRICTED")
        default:
            print("Location Services access unknown")

        }
        // [[NSNotificationCenter defaultCenter] postNotificationName:@"kCLAuthorizationStatusAuthorized" object:self];
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        //    NSLog(@"Got new locations: %@", locations);
        
//        //Add new location(s)
//        for (CLLocation* newLoc in locations) {
//            OQLocation* oqLoc = [OQLocation locationWithCLLocation:newLoc];
//            [self.locationHistory addObject:oqLoc];
//        }
//        
//        //Purge any expired locations (NB! not the most efficient code)
//        while (self.locationHistory.count > self.maxLocationHistory) {
//            [self.locationHistory removeObjectAtIndex:0];
//        }
//        
//        //Analyze new history
//        [self reanalyzeLocationHistory];
//        
//        //Always post NSNotifications on the MainThread.  You have been warned!
//        [[NSNotificationCenter defaultCenter] performSelectorOnMainThread:@selector(postNotificationName:) withObject:@"OQLocationManagerAddedLocations" waitUntilDone:NO];
        
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        //Need to react to errors here in case our geo-fences don't work or user has disabled some forms of tracking.
        //CLError
//        DDLogTopic(.CoreLocation, "Got location error: %@", error);
        print("Got location error: \(error)");
    }
    
    /*
    - (void)locationManager:(CLLocationManager *)manager
           didFailWithError:(NSError *)error {
    }
    */
    
    var currentLocation: CLLocation? {
        return self.locationManager.location
    }
}

